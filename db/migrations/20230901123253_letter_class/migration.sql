/*
  Warnings:

  - Added the required column `class` to the `Letter` table without a default value. This is not possible if the table is not empty.
  - Added the required column `meaning` to the `Letter` table without a default value. This is not possible if the table is not empty.
  - Added the required column `nameRTGS` to the `Letter` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Letter" ADD COLUMN     "class" TEXT NOT NULL,
ADD COLUMN     "meaning" TEXT NOT NULL,
ADD COLUMN     "nameRTGS" TEXT NOT NULL;
