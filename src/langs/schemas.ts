import { z } from "zod"

export const CreateLangSchema = z.object({
  name: z.string(),
  // template: __fieldName__: z.__zodType__(),
})
export const UpdateLangSchema = z.object({
  id: z.number(),
  // template: __fieldName__: z.__zodType__(),
})

export const DeleteLangSchema = z.object({
  id: z.number(),
})
