import { paginate } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db, { Prisma } from "db"

interface GetLangsInput
  extends Pick<Prisma.LangFindManyArgs, "where" | "orderBy" | "skip" | "take"> {}

export default resolver.pipe(
  resolver.authorize(),
  async ({ where, orderBy, skip = 0, take = 100 }: GetLangsInput) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const {
      items: langs,
      hasMore,
      nextPage,
      count,
    } = await paginate({
      skip,
      take,
      count: () => db.lang.count({ where }),
      query: (paginateArgs) => db.lang.findMany({ ...paginateArgs, where, orderBy }),
    })

    return {
      langs,
      nextPage,
      hasMore,
      count,
    }
  }
)
