import { resolver } from "@blitzjs/rpc"
import db from "db"
import { CreateLangSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(CreateLangSchema),
  resolver.authorize(),
  async (input) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const lang = await db.lang.create({ data: input })

    return lang
  }
)
