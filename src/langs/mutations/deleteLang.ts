import { resolver } from "@blitzjs/rpc"
import db from "db"
import { DeleteLangSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(DeleteLangSchema),
  resolver.authorize(),
  async ({ id }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const lang = await db.lang.deleteMany({ where: { id } })

    return lang
  }
)
