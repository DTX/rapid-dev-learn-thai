import { resolver } from "@blitzjs/rpc"
import db from "db"
import { UpdateLangSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(UpdateLangSchema),
  resolver.authorize(),
  async ({ id, ...data }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const lang = await db.lang.update({ where: { id }, data })

    return lang
  }
)
