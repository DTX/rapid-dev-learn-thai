import { BlitzAPIHandler } from "@blitzjs/next"
import db from "db"
import { LETTERS_ALPHABET } from "src/letters/resources/alphabet"

//ก;ก ไก่;ko kai;chicken;k;k;/k/;/k/;mid
//0;1   ;2     ;3      ;

const handler: BlitzAPIHandler<any> = async (req, res) => {
  let thaiLang = await getThaiLang()

  if (
    (await db.letter.count({
      where: { lang: thaiLang },
    })) > 0
  )
    return res.send("Already populated")

  const csv = LETTERS_ALPHABET

  await db.letter.createMany({
    data: csv.split("\n").map((line) => {
      let fields = line.split(";")
      return {
        symbol: fields[0]!,
        name: fields[1]!,
        nameRTGS: fields[2]!,
        meaning: fields[3]!,
        initialPron: fields[4]!,
        finalPron: fields[5]!,
        class: fields[8]!,
        langId: thaiLang.id,
      }
    }),
  })

  res.setHeader("Content-Type", "text/html; charset=UTF-8")
  res.json(await db.letter.findMany())
}

async function getThaiLang() {
  return await db.lang.findFirstOrThrow({ where: { name: "Thai" } })
}
export default handler
