import { BlitzAPIHandler } from "@blitzjs/next"
import db from "db"
import { LETTERS_FREQ } from "src/letters/resources/frequency"

const handler: BlitzAPIHandler<any> = async (req, res) => {
  let letters = await db.letter.findMany()

  let re = /<span.*?title="(.*?)%".*?>(.*?)<\/span/gis

  let m: RegExpExecArray | null
  // let i = 0
  // while ((m = re.exec(LETTERS_FREQ)) && i++ < 2) {
  while ((m = re.exec(LETTERS_FREQ))) {
    let target = letters.find((e) => e.symbol == m![2])
    console.log(m[1], m[2], target ? "found" : "not found")
    let freq = Number(m[1])
    if (!freq || !target) continue
    await db.letter.update({
      data: { frequency: freq },
      where: { id: target.id },
    })
  }

  res.json(
    await db.letter.findMany({
      select: { symbol: true, frequency: true },
      orderBy: { frequency: "desc" },
      where: { frequency: { not: null } },
    })
  )
}

export default handler
