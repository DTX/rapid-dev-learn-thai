import { BlitzAPIHandler } from "@blitzjs/next"
import db from "db"

const handler: BlitzAPIHandler<any> = async (req, res) => {
  // res.json(await db.letter.findMany())
  let letters = await db.letter.findMany()

  let first = letters[0]!
  res.setHeader("Content-Type", "text/html; charset=UTF-8")
  // res.send("1" + JSON.stringify(first))
  res.send("2" + first.name)

  // res.send(letters[0]!.name)
}

export default handler
