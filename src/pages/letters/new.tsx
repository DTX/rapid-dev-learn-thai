import { Routes } from "@blitzjs/next"
import Link from "next/link"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import { CreateLetterSchema } from "src/letters/schemas"
import createLetter from "src/letters/mutations/createLetter"
import { LetterForm, FORM_ERROR } from "src/letters/components/LetterForm"
import { Suspense } from "react"

const NewLetterPage = () => {
  const router = useRouter()
  const [createLetterMutation] = useMutation(createLetter)

  return (
    <Layout title={"Create New Letter"}>
      <h1>Create New Letter</h1>
      <Suspense fallback={<div>Loading...</div>}>
        <LetterForm
          submitText="Create Letter"
          schema={CreateLetterSchema}
          // initialValues={{}}
          onSubmit={async (values) => {
            try {
              const letter = await createLetterMutation(values)
              await router.push(Routes.ShowLetterPage({ letterId: letter.id }))
            } catch (error: any) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </Suspense>
      <p>
        <Link href={Routes.LettersPage()}>Letters</Link>
      </p>
    </Layout>
  )
}

NewLetterPage.authenticate = true

export default NewLetterPage
