import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"

import Layout from "src/core/layouts/Layout"
import { UpdateLetterSchema } from "src/letters/schemas"
import getLetter from "src/letters/queries/getLetter"
import updateLetter from "src/letters/mutations/updateLetter"
import { LetterForm, FORM_ERROR } from "src/letters/components/LetterForm"

export const EditLetter = () => {
  const router = useRouter()
  const letterId = useParam("letterId", "number")
  const [letter, { setQueryData }] = useQuery(
    getLetter,
    { id: letterId },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateLetterMutation] = useMutation(updateLetter)

  return (
    <>
      <Head>
        <title>Edit Letter {letter.id}</title>
      </Head>

      <div>
        <h1>Edit Letter {letter.id}</h1>
        <pre>{JSON.stringify(letter, null, 2)}</pre>
        <Suspense fallback={<div>Loading...</div>}>
          <LetterForm
            submitText="Update Letter"
            schema={UpdateLetterSchema}
            initialValues={letter}
            onSubmit={async (values) => {
              try {
                const updated = await updateLetterMutation({
                  ...values,
                  id: letter.id,
                })
                await setQueryData(updated)
                await router.push(Routes.ShowLetterPage({ letterId: updated.id }))
              } catch (error) {
                console.error(error)
                return {
                  [FORM_ERROR]: error.toString(),
                }
              }
            }}
          />
        </Suspense>
      </div>
    </>
  )
}

const EditLetterPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditLetter />
      </Suspense>

      <p>
        <Link href={Routes.LettersPage()}>Letters</Link>
      </p>
    </div>
  )
}

EditLetterPage.authenticate = true
EditLetterPage.getLayout = (page) => <Layout>{page}</Layout>

export default EditLetterPage
