import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"

import Layout from "src/core/layouts/Layout"
import getLetter from "src/letters/queries/getLetter"
import deleteLetter from "src/letters/mutations/deleteLetter"

export const Letter = () => {
  const router = useRouter()
  const letterId = useParam("letterId", "number")
  const [deleteLetterMutation] = useMutation(deleteLetter)
  const [letter] = useQuery(getLetter, { id: letterId })

  return (
    <>
      <Head>
        <title>Letter {letter.id}</title>
      </Head>

      <div>
        <h1>Letter {letter.id}</h1>
        <pre>{JSON.stringify(letter, null, 2)}</pre>

        <Link href={Routes.EditLetterPage({ letterId: letter.id })}>Edit</Link>

        <button
          type="button"
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteLetterMutation({ id: letter.id })
              await router.push(Routes.LettersPage())
            }
          }}
          style={{ marginLeft: "0.5rem" }}
        >
          Delete
        </button>
      </div>
    </>
  )
}

const ShowLetterPage = () => {
  return (
    <div>
      <p>
        <Link href={Routes.LettersPage()}>Letters</Link>
      </p>

      <Suspense fallback={<div>Loading...</div>}>
        <Letter />
      </Suspense>
    </div>
  )
}

ShowLetterPage.authenticate = true
ShowLetterPage.getLayout = (page) => <Layout>{page}</Layout>

export default ShowLetterPage
