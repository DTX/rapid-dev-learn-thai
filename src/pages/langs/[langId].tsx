import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"

import Layout from "src/core/layouts/Layout"
import getLang from "src/langs/queries/getLang"
import deleteLang from "src/langs/mutations/deleteLang"

export const Lang = () => {
  const router = useRouter()
  const langId = useParam("langId", "number")
  const [deleteLangMutation] = useMutation(deleteLang)
  const [lang] = useQuery(getLang, { id: langId })

  return (
    <>
      <Head>
        <title>Lang {lang.id}</title>
      </Head>

      <div>
        <h1>Lang {lang.id}</h1>
        <pre>{JSON.stringify(lang, null, 2)}</pre>

        <Link href={Routes.EditLangPage({ langId: lang.id })}>Edit</Link>

        <button
          type="button"
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteLangMutation({ id: lang.id })
              await router.push(Routes.LangsPage())
            }
          }}
          style={{ marginLeft: "0.5rem" }}
        >
          Delete
        </button>
      </div>
    </>
  )
}

const ShowLangPage = () => {
  return (
    <div>
      <p>
        <Link href={Routes.LangsPage()}>Langs</Link>
      </p>

      <Suspense fallback={<div>Loading...</div>}>
        <Lang />
      </Suspense>
    </div>
  )
}

ShowLangPage.authenticate = true
ShowLangPage.getLayout = (page) => <Layout>{page}</Layout>

export default ShowLangPage
