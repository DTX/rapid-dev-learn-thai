import { Routes } from "@blitzjs/next"
import Link from "next/link"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import { CreateLangSchema } from "src/langs/schemas"
import createLang from "src/langs/mutations/createLang"
import { LangForm, FORM_ERROR } from "src/langs/components/LangForm"
import { Suspense } from "react"

const NewLangPage = () => {
  const router = useRouter()
  const [createLangMutation] = useMutation(createLang)

  return (
    <Layout title={"Create New Lang"}>
      <h1>Create New Lang</h1>
      <Suspense fallback={<div>Loading...</div>}>
        <LangForm
          submitText="Create Lang"
          schema={CreateLangSchema}
          // initialValues={{}}
          onSubmit={async (values) => {
            try {
              const lang = await createLangMutation(values)
              await router.push(Routes.ShowLangPage({ langId: lang.id }))
            } catch (error: any) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </Suspense>
      <p>
        <Link href={Routes.LangsPage()}>Langs</Link>
      </p>
    </Layout>
  )
}

NewLangPage.authenticate = true

export default NewLangPage
