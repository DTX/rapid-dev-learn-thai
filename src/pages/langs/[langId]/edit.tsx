import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"

import Layout from "src/core/layouts/Layout"
import { UpdateLangSchema } from "src/langs/schemas"
import getLang from "src/langs/queries/getLang"
import updateLang from "src/langs/mutations/updateLang"
import { LangForm, FORM_ERROR } from "src/langs/components/LangForm"

export const EditLang = () => {
  const router = useRouter()
  const langId = useParam("langId", "number")
  const [lang, { setQueryData }] = useQuery(
    getLang,
    { id: langId },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateLangMutation] = useMutation(updateLang)

  return (
    <>
      <Head>
        <title>Edit Lang {lang.id}</title>
      </Head>

      <div>
        <h1>Edit Lang {lang.id}</h1>
        <pre>{JSON.stringify(lang, null, 2)}</pre>
        <Suspense fallback={<div>Loading...</div>}>
          <LangForm
            submitText="Update Lang"
            schema={UpdateLangSchema}
            initialValues={lang}
            onSubmit={async (values) => {
              try {
                const updated = await updateLangMutation({
                  ...values,
                  id: lang.id,
                })
                await setQueryData(updated)
                await router.push(Routes.ShowLangPage({ langId: updated.id }))
              } catch (error) {
                console.error(error)
                return {
                  [FORM_ERROR]: error.toString(),
                }
              }
            }}
          />
        </Suspense>
      </div>
    </>
  )
}

const EditLangPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditLang />
      </Suspense>

      <p>
        <Link href={Routes.LangsPage()}>Langs</Link>
      </p>
    </div>
  )
}

EditLangPage.authenticate = true
EditLangPage.getLayout = (page) => <Layout>{page}</Layout>

export default EditLangPage
