import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getLangs from "src/langs/queries/getLangs"

const ITEMS_PER_PAGE = 100

export const LangsList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ langs, hasMore }] = usePaginatedQuery(getLangs, {
    orderBy: { id: "asc" },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
  const goToNextPage = () => router.push({ query: { page: page + 1 } })

  return (
    <div>
      <ul>
        {langs.map((lang) => (
          <li key={lang.id}>
            <Link href={Routes.ShowLangPage({ langId: lang.id })}>{lang.name}</Link>
          </li>
        ))}
      </ul>

      <button disabled={page === 0} onClick={goToPreviousPage}>
        Previous
      </button>
      <button disabled={!hasMore} onClick={goToNextPage}>
        Next
      </button>
    </div>
  )
}

const LangsPage = () => {
  return (
    <Layout>
      <Head>
        <title>Langs</title>
      </Head>

      <div>
        <p>
          <Link href={Routes.NewLangPage()}>Create Lang</Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <LangsList />
        </Suspense>
      </div>
    </Layout>
  )
}

export default LangsPage
