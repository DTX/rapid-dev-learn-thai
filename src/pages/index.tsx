import { createContext, Suspense, useContext, useMemo } from "react"
import { BlitzPage, Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery, useQuery, invokeWithCtx } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getLetters from "src/letters/queries/getLetters"
import getAllLetters from "src/letters/queries/getAllLetters"
import { LetterCard } from "src/letters/components/LetterCard"
import { AlphabetSorter, AlphabetSortOption } from "src/letters/components/AlphabetSorter"
import { Letter } from "@prisma/client"
import { atom, useAtom } from "jotai"
import _ from "lodash"
import { getSession } from "@blitzjs/auth"
import safeJsonStringify from "safe-json-stringify"

const sortAtom = atom<AlphabetSortOption>("Initial")

export const InitialContext = createContext<TInitial>({} as any)
export async function getServerSideProps(ctx) {
  let [iniLetters] = await Promise.all([invokeWithCtx(getAllLetters, {}, ctx)])

  let toReturn = { props: { iniLetters } }
  return JSON.parse(safeJsonStringify(toReturn)) as typeof toReturn
  // return { props: JSON.parse(safeJsonStringify({ iniLetters })) }
}
type TInitial = NonNullable<Awaited<ReturnType<typeof getServerSideProps>>["props"]>

export const AlphabetBoard = () => {
  const router = useRouter()
  // const page = Number(router.query.page) || 0;
  let ini = useContext(InitialContext)
  const [letters] = useQuery(
    getAllLetters,
    { orderBy: { id: "asc" } },
    { initialData: ini.iniLetters }
  )

  const sortedLetters = useSortedLetters(letters)

  return (
    <div className="grid grid-cols-4 gap-4 p-4">
      {sortedLetters.map((letter) => (
        <LetterCard key={letter.id} letter={letter} />
      ))}
    </div>
  )
}

function useSortedLetters(letters: Letter[]) {
  let [sort] = useAtom(sortAtom)
  return useMemo(() => {
    let sorted = letters
    switch (sort) {
      case "Frequency":
        sorted = _.sortBy(letters, (e) => -Number(e.frequency))
        break
      case "Final sound":
        sorted = _.sortBy(letters, (e) => e.finalPron)
        break
      case "Initial sound":
        sorted = _.sortBy(letters, (e) => e.initialPron)
        break
      case "Name":
        sorted = _.sortBy(letters, (e) => e.nameRTGS)
        break

      default:
        break
    }
    return sorted
  }, [sort, letters])
}

const AlphabetPage: BlitzPage = (props: TInitial) => {
  console.log("Page loaded", props)
  const [, setSort] = useAtom(sortAtom)
  return (
    <Layout>
      <Head>
        <title>Alphabet</title>
      </Head>

      <div>
        <AlphabetSorter onChange={(v) => setSort(v)} />
        <Suspense fallback={<div>Loading...</div>}>
          <InitialContext.Provider value={props}>
            <AlphabetBoard />
          </InitialContext.Provider>
        </Suspense>
      </div>
    </Layout>
  )
}

export default AlphabetPage
