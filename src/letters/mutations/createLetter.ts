import { resolver } from "@blitzjs/rpc"
import db from "db"
import { CreateLetterSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(CreateLetterSchema),
  resolver.authorize(),
  async (input) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const letter = await db.letter.create({ data: input as any })

    return letter
  }
)
