import { resolver } from "@blitzjs/rpc"
import db from "db"
import { DeleteLetterSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(DeleteLetterSchema),
  resolver.authorize(),
  async ({ id }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const letter = await db.letter.deleteMany({ where: { id } })

    return letter
  }
)
