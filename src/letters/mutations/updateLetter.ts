import { resolver } from "@blitzjs/rpc"
import db from "db"
import { UpdateLetterSchema } from "../schemas"

export default resolver.pipe(
  resolver.zod(UpdateLetterSchema),
  resolver.authorize(),
  async ({ id, ...data }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const letter = await db.letter.update({ where: { id }, data })

    return letter
  }
)
