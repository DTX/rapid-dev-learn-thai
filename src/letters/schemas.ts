import { z } from "zod"

export const CreateLetterSchema = z.object({
  name: z.string(),
  // template: __fieldName__: z.__zodType__(),
})
export const UpdateLetterSchema = z.object({
  id: z.number(),
  // template: __fieldName__: z.__zodType__(),
})

export const DeleteLetterSchema = z.object({
  id: z.number(),
})
