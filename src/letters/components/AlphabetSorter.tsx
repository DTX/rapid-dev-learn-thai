import { Letter } from "db"
import React, { Suspense, useState } from "react"

interface LAlphabetSorterProps {
  sortBy?: AlphabetSortOption
  onChange?: (value: AlphabetSortOption) => void
}

export type AlphabetSortOption = "Initial" | "Frequency" | "Final sound" | "Initial sound" | "Name"

export function AlphabetSorter({ sortBy = "Initial", onChange }: LAlphabetSorterProps) {
  let allOptions: AlphabetSortOption[] = [
    "Initial",
    "Frequency",
    "Final sound",
    "Initial sound",
    "Name",
  ]
  let [currentValue, setCurVal] = useState(sortBy)

  const doChangeSortBy = (value: AlphabetSortOption) => {
    setCurVal(value)
    onChange?.(value)
  }

  return (
    <div className="bg-emerald-200 p-4">
      <div>Sort by</div>
      <div className="grid grid-cols-2">
        {allOptions.map((option) => (
          <label key={option}>
            <input
              type="radio"
              name="alphabet-sorter"
              checked={currentValue == option}
              onChange={() => doChangeSortBy(option)}
              className="mr-2"
            />
            {option}
          </label>
        ))}
      </div>
    </div>
  )
}
