import { Letter } from "db"
import React, { Suspense } from "react"

interface LetterCardProps {
  letter: Letter
}

export function LetterCard({ letter }: LetterCardProps) {
  return (
    <div className="relative text-center bg-gray-300 overflow-hidden rounded-xl whitespace-nowrap">
      {letter.frequency && (
        <div className="absolute right-0 pr-2">{+letter.frequency?.toFixed(2)}</div>
      )}
      <div className="text-9xl pb-4">{letter.symbol}</div>
      <div className="text-3xl">{letter.name}</div>
      <div>{letter.nameRTGS}</div>
      <div>({letter.meaning})</div>
      <div>
        {letter.initialPron}/{letter.finalPron}
      </div>
      <div className="bg-gray-400">{letter.class}</div>
    </div>
  )
}
