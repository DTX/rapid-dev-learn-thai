import { paginate } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db, { Prisma } from "db"

interface GetLettersInput
  extends Pick<Prisma.LetterFindManyArgs, "where" | "orderBy" | "skip" | "take"> {}

export default resolver.pipe(
  resolver.authorize(),
  async ({ where, orderBy, skip = 0, take = 100 }: GetLettersInput) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const {
      items: letters,
      hasMore,
      nextPage,
      count,
    } = await paginate({
      skip,
      take,
      count: () => db.letter.count({ where }),
      query: (paginateArgs) => db.letter.findMany({ ...paginateArgs, where, orderBy }),
    })

    return {
      letters,
      nextPage,
      hasMore,
      count,
    }
  }
)
