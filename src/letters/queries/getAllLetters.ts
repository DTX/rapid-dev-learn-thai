import { paginate } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db, { Letter, Prisma } from "db"

interface GetLettersInput
  extends Pick<Prisma.LetterFindManyArgs, "where" | "orderBy" | "skip" | "take"> {}

export default resolver.pipe(
  // resolver.authorize(),
  async ({ where, orderBy }: GetLettersInput) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    return await db.letter.findMany({ where, orderBy })
  }
)
