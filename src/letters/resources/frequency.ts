export const LETTERS_FREQ = `<td style="vertical-align: middle; padding-left: 1em;">
<span
   title="6.3%"
   style="font-size: 381%; display: inline-block; vertical-align: middle;"
   >า</span
><span
   title="5.65%"
   style="font-size: 357%; display: inline-block; vertical-align: middle;"
   >น</span
><span
   title="5.17%"
   style="font-size: 338%; display: inline-block; vertical-align: middle;"
   >ร</span
><span
   title="4.02%"
   style="font-size: 291%; display: inline-block; vertical-align: middle;"
   >อ</span
><span
   title="3.94%"
   style="font-size: 287%; display: inline-block; vertical-align: middle;"
   >ก</span
><span
   title="3.8%"
   style="font-size: 281%; display: inline-block; vertical-align: middle;"
   >เ</span
><span
   title="3.72%"
   style="font-size: 278%; display: inline-block; vertical-align: middle;"
   >ง</span
><span
   title="3.71%"
   style="font-size: 277%; display: inline-block; vertical-align: middle;"
   >่</span
><span
   title="3.2%"
   style="font-size: 253%; display: inline-block; vertical-align: middle;"
   >ม</span
><span
   title="3.19%"
   style="font-size: 253%; display: inline-block; vertical-align: middle;"
   >ั</span
><span
   title="2.77%"
   style="font-size: 233%; display: inline-block; vertical-align: middle;"
   >้</span
><span
   title="2.71%"
   style="font-size: 230%; display: inline-block; vertical-align: middle;"
   >ี</span
><span
   title="2.59%"
   style="font-size: 223%; display: inline-block; vertical-align: middle;"
   >ย</span
><span
   title="2.5%"
   style="font-size: 219%; display: inline-block; vertical-align: middle;"
   >ล</span
><span
   title="2.4%"
   style="font-size: 213%; display: inline-block; vertical-align: middle;"
   >ว</span
><span
   title="2.27%"
   style="font-size: 206%; display: inline-block; vertical-align: middle;"
   >ด</span
><span
   title="2.19%"
   style="font-size: 202%; display: inline-block; vertical-align: middle;"
   >ท</span
><span
   title="2.08%"
   style="font-size: 196%; display: inline-block; vertical-align: middle;"
   >ิ</span
><span
   title="1.99%"
   style="font-size: 191%; display: inline-block; vertical-align: middle;"
   >ส</span
><span
   title="1.91%"
   style="font-size: 186%; display: inline-block; vertical-align: middle;"
   >ต</span
><span
   title="1.82%"
   style="font-size: 181%; display: inline-block; vertical-align: middle;"
   >ะ</span
><span
   title="1.79%"
   style="font-size: 179%; display: inline-block; vertical-align: middle;"
   >ป</span
><span
   title="1.67%"
   style="font-size: 172%; display: inline-block; vertical-align: middle;"
   >บ</span
><span
   title="1.56%"
   style="font-size: 165%; display: inline-block; vertical-align: middle;"
   >ค</span
><span
   title="1.54%"
   style="font-size: 163%; display: inline-block; vertical-align: middle;"
   >ห</span
><span
   title="1.48%"
   style="font-size: 159%; display: inline-block; vertical-align: middle;"
   >แ</span
><span
   title="1.35%"
   style="font-size: 151%; display: inline-block; vertical-align: middle;"
   >จ</span
><span
   title="1.32%"
   style="font-size: 149%; display: inline-block; vertical-align: middle;"
   >พ</span
><span
   title="1.09%"
   style="font-size: 133%; display: inline-block; vertical-align: middle;"
   >ช</span
><span
   title="1.07%"
   style="font-size: 131%; display: inline-block; vertical-align: middle;"
   >์</span
><span
   title="1.06%"
   style="font-size: 130%; display: inline-block; vertical-align: middle;"
   >ข</span
><span
   title="1.05%"
   style="font-size: 130%; display: inline-block; vertical-align: middle;"
   >ใ</span
><span
   title="0.961%"
   style="font-size: 123%; display: inline-block; vertical-align: middle;"
   >ื</span
><span
   title="0.913%"
   style="font-size: 119%; display: inline-block; vertical-align: middle;"
   >ไ</span
><span
   title="0.897%"
   style="font-size: 118%; display: inline-block; vertical-align: middle;"
   >ุ</span
><span
   title="0.875%"
   style="font-size: 116%; display: inline-block; vertical-align: middle;"
   >็</span
><span
   title="0.781%"
   style="font-size: 109%; display: inline-block; vertical-align: middle;"
   >ู</span
><span
   title="0.776%"
   style="font-size: 108%; display: inline-block; vertical-align: middle;"
   >โ</span
><span
   title="0.697%"
   style="font-size: 102%; display: inline-block; vertical-align: middle;"
   >ศ</span
><span
   title="0.642%"
   style="font-size: 97%; display: inline-block; vertical-align: middle;"
   >ำ</span
><span
   title="0.528%"
   style="font-size: 86%; display: inline-block; vertical-align: middle;"
   >ึ</span
><span
   title="0.442%"
   style="font-size: 77%; display: inline-block; vertical-align: middle;"
   >ซ</span
><span
   title="0.414%"
   style="font-size: 74%; display: inline-block; vertical-align: middle;"
   >e</span
><span
   title="0.409%"
   style="font-size: 74%; display: inline-block; vertical-align: middle;"
   >2</span
><span
   title="0.402%"
   style="font-size: 73%; display: inline-block; vertical-align: middle;"
   >ถ</span
><span
   title="0.381%"
   style="font-size: 71%; display: inline-block; vertical-align: middle;"
   >a</span
><span
   title="0.352%"
   style="font-size: 67%; display: inline-block; vertical-align: middle;"
   >1</span
><span
   title="0.349%"
   style="font-size: 67%; display: inline-block; vertical-align: middle;"
   >ธ</span
><span
   title="0.335%"
   style="font-size: 65%; display: inline-block; vertical-align: middle;"
   >ษ</span
><span
   title="0.329%"
   style="font-size: 65%; display: inline-block; vertical-align: middle;"
   >ภ</span
><span
   title="0.324%"
   style="font-size: 64%; display: inline-block; vertical-align: middle;"
   >ณ</span
><span
   title="0.308%"
   style="font-size: 62%; display: inline-block; vertical-align: middle;"
   >i</span
><span
   title="0.301%"
   style="font-size: 61%; display: inline-block; vertical-align: middle;"
   >ผ</span
><span
   title="0.292%"
   style="font-size: 60%; display: inline-block; vertical-align: middle;"
   >o</span
><span
   title="0.29%"
   style="font-size: 60%; display: inline-block; vertical-align: middle;"
   >0</span
><span
   title="0.281%"
   style="font-size: 59%; display: inline-block; vertical-align: middle;"
   >n</span
><span
   title="0.28%"
   style="font-size: 59%; display: inline-block; vertical-align: middle;"
   >t</span
><span
   title="0.278%"
   style="font-size: 59%; display: inline-block; vertical-align: middle;"
   >r</span
><span
   title="0.273%"
   style="font-size: 58%; display: inline-block; vertical-align: middle;"
   >5</span
><span
   title="0.253%"
   style="font-size: 55%; display: inline-block; vertical-align: middle;"
   >ญ</span
><span
   title="0.245%"
   style="font-size: 54%; display: inline-block; vertical-align: middle;"
   >s</span
><span
   title="0.198%"
   style="font-size: 48%; display: inline-block; vertical-align: middle;"
   >l</span
><span
   title="0.191%"
   style="font-size: 47%; display: inline-block; vertical-align: middle;"
   >ฟ</span
><span
   title="0.186%"
   style="font-size: 46%; display: inline-block; vertical-align: middle;"
   >4</span
><span
   title="0.168%"
   style="font-size: 43%; display: inline-block; vertical-align: middle;"
   >3</span
><span
   title="0.167%"
   style="font-size: 43%; display: inline-block; vertical-align: middle;"
   >c</span
><span
   title="0.147%"
   style="font-size: 40%; display: inline-block; vertical-align: middle;"
   >9</span
><span
   title="0.139%"
   style="font-size: 39%; display: inline-block; vertical-align: middle;"
   >h</span
><span
   title="0.137%"
   style="font-size: 38%; display: inline-block; vertical-align: middle;"
   >m</span
><span
   title="0.136%"
   style="font-size: 38%; display: inline-block; vertical-align: middle;"
   >d</span
><span
   title="0.128%"
   style="font-size: 37%; display: inline-block; vertical-align: middle;"
   >ฐ</span
><span
   title="0.126%"
   style="font-size: 36%; display: inline-block; vertical-align: middle;"
   >u</span
><span
   title="0.111%"
   style="font-size: 34%; display: inline-block; vertical-align: middle;"
   >8</span
><span
   title="0.108%"
   style="font-size: 33%; display: inline-block; vertical-align: middle;"
   >p</span
><span
   title="0.104%"
   style="font-size: 32%; display: inline-block; vertical-align: middle;"
   >6</span
><span
   title="0.1%"
   style="font-size: 32%; display: inline-block; vertical-align: middle;"
   >g</span
><span
   title="0.1%"
   style="font-size: 32%; display: inline-block; vertical-align: middle;"
   >7</span
><span
   title="0.0855%"
   style="font-size: 29%; display: inline-block; vertical-align: middle;"
   >ฮ</span
><span
   title="0.0787%"
   style="font-size: 27%; display: inline-block; vertical-align: middle;"
   >ๆ</span
><span
   title="0.0765%"
   style="font-size: 27%; display: inline-block; vertical-align: middle;"
   >b</span
><span
   title="0.075%"
   style="font-size: 27%; display: inline-block; vertical-align: middle;"
   >ฤ</span
><span
   title="0.0742%"
   style="font-size: 27%; display: inline-block; vertical-align: middle;"
   >ฉ</span
><span
   title="0.0737%"
   style="font-size: 26%; display: inline-block; vertical-align: middle;"
   >f</span
><span
   title="0.0687%"
   style="font-size: 25%; display: inline-block; vertical-align: middle;"
   >y</span
><span
   title="0.0685%"
   style="font-size: 25%; display: inline-block; vertical-align: middle;"
   >ฝ</span
><span
   title="0.0516%"
   style="font-size: 21%; display: inline-block; vertical-align: middle;"
   >w</span
><span
   title="0.0482%"
   style="font-size: 20%; display: inline-block; vertical-align: middle;"
   >k</span
><span
   title="0.0467%"
   style="font-size: 20%; display: inline-block; vertical-align: middle;"
   >v</span
><span
   title="0.0438%"
   style="font-size: 19%; display: inline-block; vertical-align: middle;"
   >ฏ</span
><span
   title="0.0376%"
   style="font-size: 18%; display: inline-block; vertical-align: middle;"
   >ฎ</span
><span
   title="0.034%"
   style="font-size: 17%; display: inline-block; vertical-align: middle;"
   >ฒ</span
><span
   title="0.0236%"
   style="font-size: 13%; display: inline-block; vertical-align: middle;"
   >“</span
><span
   title="0.0235%"
   style="font-size: 13%; display: inline-block; vertical-align: middle;"
   >”</span
><span
   title="0.0223%"
   style="font-size: 13%; display: inline-block; vertical-align: middle;"
   >๊</span
><span
   title="0.0217%"
   style="font-size: 13%; display: inline-block; vertical-align: middle;"
   >ฬ</span
><span
   title="0.0212%"
   style="font-size: 13%; display: inline-block; vertical-align: middle;"
   >ฑ</span
><span
   title="0.0207%"
   style="font-size: 12%; display: inline-block; vertical-align: middle;"
   >j</span
><span
   title="0.0196%"
   style="font-size: 12%; display: inline-block; vertical-align: middle;"
   >ฯ</span
><span
   title="0.0193%"
   style="font-size: 12%; display: inline-block; vertical-align: middle;"
   >x</span
><span
   title="0.0169%"
   style="font-size: 11%; display: inline-block; vertical-align: middle;"
   >ฆ</span
>
</td>
`
